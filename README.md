# Portfolio

## Objectif
1. la création d'un portefeuille suivant le modèle wirefeame initialement créé
2. N'utilisez que du html et du css
3. Faites en sorte que le code soit aussi correct, compact et adaptable que possible.
4. Et qu'il soit responsive

## Todo
1. Header --> DONE
2. Header style -->DONE
3. Main --> DONE
4. Main style --> DONE
5. Footer --> DONE
6. Footer style --> DONE
7. Responsive --> DONE
8. Complication de Scss à css --> DONE

## Fonctionnement
1. Ouvrez le fichier index.html pour afficher le contenu
2. Vous pouvez également accéder au profil en ligne via git lab : https://mustaelhachmimahti.gitlab.io/porfolio/ u git hub : https://soymustamahti.github.io/.
3. Vous pouvez également le consulter sur votre téléphone portable car il est responsive.

## Continuer à développer
1. npm i (pour installer les bibliothèques nécessaires)
2. npm run comp (pour compiler style.scss en style.css)
